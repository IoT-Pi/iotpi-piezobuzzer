# PiezoBuzzer IoT-Pi Module
Simple module to control a PiezoBuzzer

## Installation

1. Put `piezobuzzer.py` in `iotpi/modules/` folder
2. Import the module in `iotpi/init.py`:
    ```python
    import iotpi.modules.piezobuzzer as piezobuzzer
    self.buzzer = piezobuzzer.module(14) # 14 = Pin
