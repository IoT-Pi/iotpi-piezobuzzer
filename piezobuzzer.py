import pigpio
import time
from multiprocessing import Process

class module():
    def on(self, freq):
        self.gpio.set_PWM_dutycycle(self.pin, freq)

    def off(self):
        self.gpio.set_PWM_dutycycle(self.pin, 0)
        
    def beep(self, freq, dur):
        def prcs(self, freq, dur):
            self.gpio.set_PWM_dutycycle(self.pin, freq)
            time.sleep(dur)
            self.gpio.set_PWM_dutycycle(self.pin, 0)
        p = Process(target=prcs, args=(self, freq, dur,))
        p.start()

    def __init__(self, pin):
        self.pin = pin
        self.gpio = pigpio.pi()
        self.gpio.set_PWM_dutycycle(self.pin, 0)
